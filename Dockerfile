FROM rust:1.54 as builder
WORKDIR /usr/src/ownroll
COPY . .
RUN cargo install --path .

FROM debian:buster-slim
RUN apt-get update && apt-get install -y ca-certificates ffmpeg && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/ownroll /usr/local/bin/ownroll
COPY --from=builder /usr/src/ownroll/static /storage/ownroll/static
WORKDIR /app
ENV RUN_ENVIRONMENT=docker
CMD ["ownroll"]