# Ownroll

Selfhosted Owncast directory written in Rust.

## Introduction

Ownroll is a curated Owncast directory.

This tool fetches the status of the Owncast instances provided in the `config.toml` file and, if online, generates a thumbnail for the streams. Then a static site is generated and the tool exits.

The idea is that the tool is run periodically by cron jobs/systemd timers/etc. and the generated website served statically. 

Ownroll needs `ffmpeg` to be installed.

## Usage

### Config

Copy the `config.template.toml` to `config.toml` and edit the new file.

### Native rust

If you have rust installed, run:

```
cargo run
```

### Docker (local build)

If you have docker installed, run:

```
docker build -t ownroll .
docker run --rm --user "$(id -u)":"$(id -g)" -v "$PWD":/usr/src/ownroll -w /usr/src/ownroll ownroll:latest ownroll
```

### Docker (dockerhub)

If you have docker installed, run:

```
docker run --rm --user "$(id -u)":"$(id -g)" -v "$PWD":/usr/src/ownroll -w /usr/src/ownroll yarmo/ownroll:latest ownroll
```

There is also an unstable "dev" channel.

## State of the project

I'm using Ownroll to learn the rust programming language and while I wouldn't publish the code if it couldn't run, do:

- expect bugs;
- expect suboptimal code;
- expect big breaking changes at any point. 

I greatly appreciate all constructive criticism and improvement tips. All contributions are welcome.

## Roadmap

- [ ] Binary builds
- [ ] Make ffmpeg optional
- [ ] ActivityPub notification posting
- [ ] Documentation
- [ ] Tests