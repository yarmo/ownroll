#![allow(non_snake_case)]

#[macro_use]
extern crate serde_derive;

mod settings;

use std::fs;
use std::path::Path;
use std::process::Command;
use std::time::Duration;

use execute::Execute;
use settings::Settings;
use settings::OwncastConfig;
use tera::Tera;
use tera::Context;
use chrono::{Local, DateTime};
use chrono::format::ParseError;
use humantime::format_duration;

#[derive(Debug, Serialize, Deserialize)]
struct OwncastServerStatus {
    online: bool,
    lastConnectTime: Option<String>,
    streamTitle: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct OwncastServerConfig {
    name: String,
    summary: String,
    logo: String,
    version: String,
    nsfw: bool,
}

#[derive(Debug, Serialize, Deserialize)]
struct OwncastInstance {
    url: String,
    hash: String,
    duration: String,
    server_status: OwncastServerStatus,
    server_config: OwncastServerConfig,
}

#[derive(Debug, Serialize, Deserialize)]
struct TemplateData {
    title: String,
    instances: Vec<OwncastInstance>,
}

fn compute_duration(timestamp: &String) -> Result<String, ParseError> {
    let now: DateTime<Local> = Local::now();
    let then = DateTime::parse_from_rfc3339(timestamp).unwrap();
    
    let interval_secs = now.signed_duration_since(then).to_std().unwrap().as_secs();

    let interval_secs_rem;
    if interval_secs > 3600*2 {
        interval_secs_rem = interval_secs % 3600;
    } else {   
        interval_secs_rem = interval_secs % 60;
    }

    let interval = Duration::from_secs(interval_secs - interval_secs_rem);
    let interval_format = format!("{}", format_duration(interval).to_string());
    Ok(interval_format)
}

async fn request_owncast_server_status(url: &str) -> Result<OwncastServerStatus, reqwest::Error> {
    let url_api = format!("{}/api/status", url);
    let resp = reqwest::get(url_api)
        .await?
        .json::<OwncastServerStatus>()
        .await?;
    Ok(resp)
}

async fn request_owncast_server_config(url: &str) -> Result<OwncastServerConfig, reqwest::Error> {
    let url_api = format!("{}/api/config", url);
    let resp = reqwest::get(url_api)
        .await?
        .json::<OwncastServerConfig>()
        .await?;
    Ok(resp)
}

fn generate_thumbnail(hash: &str, url: &str) -> Result<Option<i32>, std::io::Error> {
    let settings = Settings::new().unwrap();
    let mut test_command = Command::new(&settings.ownroll.ffmpeg_path);

    test_command.arg("-version");
    if test_command.execute_check_exit_status_code(0).is_err() {
        eprintln!("The path `{}` is not a correct FFmpeg executable binary file.", &settings.ownroll.ffmpeg_path);
    }

    let mut command = Command::new(&settings.ownroll.ffmpeg_path);
    command.arg("-y");
    command.arg("-i");
    command.arg(format!("{}/hls/stream.m3u8", url));
    command.arg("-vf");
    command.arg("scale=1280:720");
    command.arg("-vframes");
    command.arg("1");
    command.arg(format!("public/static/{}_thumbnail.png", hash));

    command.execute()
}
fn generate_logo(hash: &str, url: &str) -> Result<Option<i32>, std::io::Error> {
    let settings = Settings::new().unwrap();
    let mut test_command = Command::new(&settings.ownroll.ffmpeg_path);

    test_command.arg("-version");
    if test_command.execute_check_exit_status_code(0).is_err() {
        eprintln!("The path `{}` is not a correct FFmpeg executable binary file.", &settings.ownroll.ffmpeg_path);
    }

    let mut command = Command::new(&settings.ownroll.ffmpeg_path);
    command.arg("-y");
    command.arg("-i");
    command.arg(format!("{}/logo", url));
    command.arg("-vf");
    command.arg("scale=480:480");
    command.arg(format!("public/static/{}_logo.png", hash));

    command.execute()
}

fn generate_html(template_data: TemplateData) -> Result<Tera, Box<dyn std::error::Error>> {
    let tera = match Tera::new("templates/**/*.html") {
        Ok(t) => t,
        Err(e) => {
            println!("Parsing error(s): {}", e);
            ::std::process::exit(1);
        }
    };
    let result = tera.render("index.html", &Context::from_serialize(&template_data)?)?;
    fs::write("public/index.html", result)?;

    Ok(tera)
}

fn copy_static_files() -> Result<(), Box<dyn std::error::Error>> {
    let settings = Settings::new().unwrap();
    let paths = fs::read_dir(settings.ownroll.static_path).unwrap();

    for path in paths {
        let path_src = path.unwrap().path();
        println!("- {}", &path_src.display());
        let path_dest = Path::new("./public/static/");
        fs::copy(&path_src, &path_dest.join(&path_src.file_name().unwrap()))?;
    };

    Ok(())
}

async fn status_update() -> Result<(), Box<dyn std::error::Error>> {
    fs::create_dir_all("./public/static")?;
    
    let settings = Settings::new().unwrap();
    let instances = &settings.instances;

    let mut template_data = TemplateData {
        title: settings.ownroll.title.to_string(),
        instances: Vec::with_capacity(instances.len())
    };

    println!("Processing Owncast instances...");
    for instance_config in instances {
        let instance_config: &OwncastConfig = instance_config;
        let hash = format!("{:x}", md5::compute(&instance_config.url));
        
        let server_status = match request_owncast_server_status(&instance_config.url).await {
            Ok(t) => t,
            Err(e) => {
                println!("Parsing error(s): {}", e);
                continue;
            }
        };
        let server_config = match request_owncast_server_config(&instance_config.url).await {
            Ok(t) => t,
            Err(e) => {
                println!("Parsing error(s): {}", e);
                continue;
            }
        };
        println!("- {}", &instance_config.url);

        let duration: String = match &server_status.lastConnectTime {
            Some(x) => compute_duration(x).unwrap(),
            None    => "-".to_string(),
        };
        
        println!("  Generating a logo...");
        match generate_logo(&hash, &instance_config.url) {
            Ok(_) => println!("  Done!"),
            Err(e) => println!("  Failed: {}", e),
        };
        if server_status.online == true {
            println!("  Generating a thumbnail...");
            match generate_thumbnail(&hash, &instance_config.url) {
                Ok(_) => println!("  Done!"),
                Err(e) => println!("  Failed: {}", e),
            };
        }

        template_data.instances.push(OwncastInstance {
            hash: hash,
            url: instance_config.url.clone(),
            duration: duration,
            server_status: server_status,
            server_config: server_config
        });
    }
    println!("Done!");

    println!("Generating HTML...");
    match generate_html(template_data) {
        Ok(_) => println!("Done!"),
        Err(e) => println!("Failed: {}", e),
    };

    println!("Copying static files...");
    match copy_static_files() {
        Ok(_) => println!("Done!"),
        Err(e) => println!("Failed: {}", e),
    };

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let _res = status_update().await;

    Ok(())
}