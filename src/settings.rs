use std::env;
use config::{ConfigError, Config, File};

#[derive(Debug, Serialize, Deserialize)]
pub struct OwnrollConfig {
    pub title: String,
    pub ffmpeg_path: String,
    pub static_path: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct OwncastConfig {
    pub url: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Settings {
    pub ownroll: OwnrollConfig,
    pub instances: Vec<OwncastConfig>,
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let mut s = Config::default();
        
        let env = match env::var("RUN_ENVIRONMENT") {
            Ok(e) =>  e,
            Err(_) => "development".to_string(),
        };

        s.set_default("ownroll.title", "Ownroll")?;
        s.set_default("ownroll.ffmpeg_path", "/usr/bin/ffmpeg")?;
        s.set_default("ownroll.static_path", "./static")?;

        s.merge(File::with_name("config.toml"))?;

        // Force the docker config
        if env == "docker" {
            s.set("ownroll.ffmpeg_path", "/usr/bin/ffmpeg")?;
            s.set("ownroll.static_path", "/storage/ownroll/static")?;
        }

        s.try_into()
    }
}
